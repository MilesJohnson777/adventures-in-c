/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on January 18, 2017, 9:30 PM
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct dogFavs{
    char *food;
    char *friend;
} dogFavs;


typedef struct dog{
    const char *name;
    const char *breed;
    int avgHeight;
    int avgWeight;
    
    dogFavs favStuff;
} dog;

void getDogFavs(dog theDog){
    printf("\n");
    printf("My last dog was named %s. He was a %s. He was %dcm tall and weighted %dlbs.\n",
    theDog.name, theDog.breed, theDog.avgHeight, theDog.avgWeight);
    printf("He liked to eat %s and his only friend was %s.\n\n", theDog.favStuff.food, theDog.favStuff.friend);
}

void setDogWeightPtr(dog *theDog, int newWeight){
    theDog->avgWeight = newWeight;
    printf("The weight was changed to %d\n\n", theDog->avgWeight);
}

void main() {

    dog jasper = {
        "Jasper",
        "Pit Bull",
        78,
        90,
        {
            "kibble",
            "Franken the cat"
        }
    };
    
    getDogFavs(jasper);
    
    setDogWeightPtr(&jasper, 95);
    printf("The weight here in main() %d\n\n", jasper.avgWeight);
    
}

