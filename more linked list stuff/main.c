/* 
 * File:   main.c
 * Author: miles
 *
 * Created on February 16, 2017, 1:48 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct product{
        float price;
        char productName[30];
        struct product *next;
};

struct product *pFirstNode = NULL;
struct product *pLastNode = NULL;

void createNewList(){
        struct product *pNewStruct = (struct product *) malloc(sizeof(struct product));
    
        pNewStruct->next = NULL;
    
        printf("Enter product name: ");
        scanf("%[^\n]s", &(pNewStruct)->productName);

        printf("\nEnter product price: ");
        scanf("%f", &(pNewStruct)->price);
        printf("\n");
    
        pFirstNode = pLastNode = pNewStruct;
}

void inputData(){
        if(pFirstNode == NULL){
                createNewList();
        } else {
                struct product *pNewStruct = (struct product *) malloc(sizeof(struct product));

                printf("Enter a product name: ");
                scanf("%[^\n]s", &(pNewStruct)->productName);

                printf("Enter the price: ");
                scanf("%f", &(pNewStruct)->price);
                printf("\n");

                if(pFirstNode == pLastNode){
                        pFirstNode->next = pNewStruct;
                        pLastNode = pNewStruct;
                        pNewStruct->next = NULL;
                } else {
                        pLastNode->next = pNewStruct;
                        pNewStruct->next = NULL;
                        pLastNode = pNewStruct;
                }
        }
}

void outputData(){
        struct product *pProducts = pFirstNode;
        printf("\nProducts entered: \n\n");
    
        while(pProducts != NULL){
                printf("%s cost %.2f\n", pProducts->productName, pProducts->price);
                pProducts = pProducts->next;
        }
        printf("\n");
}

void findProduct(){
    
}

void mainMenu(){
        while(1){
                printf("*** MAIN MENU ***\n\n");
                printf("Press \'a\' to make a new product entry.\n");
                printf("Press \'b\' to search for a specific product entry.\n");
                printf("Press \'c\' to list all product entries.\n");
                printf("Press \'q\' or CTRL+C to exit\n");
                printf("Choose now: ");

                char menuOption[4];
                fgets(menuOption, sizeof(menuOption), stdin);
                char * removeNL = strrchr(menuOption, '\n');
                if(removeNL) *removeNL = '\0';
                printf("\nYou chose: \'%s\'\n", menuOption);

                if(*menuOption == 113){
                        printf("Have a great day!");
                        break;
                }
                switch(*menuOption){
                        case 97: inputData();
                                break;
                        case 98: findProduct();
                                break;
                        case 99: outputData();
                                break;
                        default: printf("\'%s\' was not a valid option, please try again.\n\n", menuOption);
                }
        
//        if(*menuOption == 113) break;
//        if(*menuOption == 97 || 98 || 99){
//              if(*menuOption == 97) inputData();
//              if(*menuOption == 98) findProduct();
//              if(*menuOption == 99) outputData();
//        }

//        printf("That was not a valid option, please try again.\n");
        
        }
}

void main(){
        mainMenu();
    
}