/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on October 14, 2016, 2:15 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
int main() {
    
    int start;
    
    printf("Please enter a number to run it through the Collatz conjecture: ");
    scanf("%d", &start);
    printf("\nFrom %d:\n\n", start);
    
    int count = 0;
    
    while(1){
        if(start % 2 == 0){
           start /= 2;
           count++;
           if(start == 1){
               printf("%d\n", start);
               break;
           }
           printf("%d, ", start);
        }
        else{
            start = start * 3 + 1;
            count++;
            if(start == 1){
                printf("%d\n", start);
                break;
            }
            printf("%d, ", start);
        }
        if(count % 4 == 0){
            printf("\n");
        }
    }

    printf("\nTotal cycles: %d\n", count);
    
    return 0;
}

