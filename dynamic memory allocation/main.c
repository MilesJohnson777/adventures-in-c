/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on February 11, 2017, 3:16 PM
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct product{
    float price;
    char productName[30];
} product;

/*
 * 
 */
int main() {

    product *pProducts;
    int i;
    int j;
    int numberOfProducts;
    
    printf("Enter the number of products to store: ");
    scanf("%d", &numberOfProducts);
    
    pProducts = (product *) malloc(numberOfProducts * sizeof(product));
    
    for(i = 0; i < numberOfProducts; ++i){
        printf("Enter a product name: ");
        scanf("%s", &(pProducts+i)->productName);
        
        printf("Enter a product price: ");
        scanf("%f", &(pProducts+i)->price);
        
    }
    
    printf("Products stored: \n");
    for(j = 0; j < numberOfProducts; ++j){
        printf("%s\t%.2f\n", (pProducts+j)->productName, (pProducts+j)->price);
        
    }
    
    free(pProducts);
    
    return 0;
}

