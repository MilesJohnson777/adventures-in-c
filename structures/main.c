/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on January 18, 2017, 8:54 PM
 */

#include <stdio.h>
#include <stdlib.h>

struct dog{
    const char *name;
    const char *breed;
    int avgHeight;
    int avgWeight;
};

void printDogInfo(struct dog theDog){
    
    printf("\n");
    
    printf("My dog's name is %s, he is a %s with a height and weight of %d cm and %d lbs.\n\n",
    theDog.name, theDog.breed, theDog.avgHeight, theDog.avgWeight);
    
}

void getMemLoc(struct dog theDog){
    
    printf("Memory locations for: %s\n", theDog.name);
    printf("Name location: %d\n", theDog.name);
    printf("Breed location: %d\n", theDog.breed);
    printf("Height location: %d\n", &theDog.avgHeight);
    printf("Weight location: %d\n\n", &theDog.avgWeight);

}

void main() {
    
    struct dog laska = {"Laska", "Dashhound", 15, 10};
    
    printDogInfo(laska);

    struct dog laska2 = laska;
    
    getMemLoc(laska);
    getMemLoc(laska2);

}

