/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on October 13, 2016, 10:59 AM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
int main() { 

    char ch;
    char original;
    printf("\nEnter an alphabetic character: ");
    scanf("%c", &ch);
    original = ch;
    int i;
    
    if(ch>='A' && ch<='Z'){
        ch = ch + 32;
        printf("\nThe lower case version of %c is %c", original, ch);
        if(original == 65){
            printf("\nHere is every letter after %c: ", original);
                for(i = original + 1; i < 91; i++){
                    if(i != 90){
                        printf("%c, ", i);
                    }
                    else{
                        printf("%c\n", i);
                    }
                }
        }   
        else{
            printf("\nHere is every letter prior to %c: ", original);
            for(i = 65; i < original; i++){
                if(i != original){
                    printf("%c, ", i);
                }
                else{
                    printf("%c\n", i);
                }
            }
            if(original != 90){
                printf("\nHere is every letter after %c: ", original);
                for(i = original + 1; i < 91; i++){
                    if(i != 90){
                        printf("%c, ", i);
                    }
                    else{
                        printf("%c\n", i);
                    }
                }
            }
        }
    } 
    else if(ch>='a' && ch<='z'){
        ch = ch - 32;
        printf("\nThe upper case version of %c is %c", original, ch);
        if(original == 97){
            printf("\nHere is every letter after %c: ", original);
                for(i = original + 1; i < 123; i++){
                    if(i != 122){
                        printf("%c, ", i);
                    }
                    else{
                        printf("%c\n", i);
                    }
                }
        }   
        else{
            printf("\nHere is every letter prior to %c: ", original);
            for(i = 97; i < original; i++){
                if(i != original){
                    printf("%c, ", i);
                }
                else{
                    printf("%c\n", i);
                }
            }
            if(original != 122){
                printf("\nHere is every letter after %c: ", original);
                for(i = original + 1; i < 123; i++){
                    if(i != 122){
                        printf("%c, ", i);
                    }
                    else{
                        printf("%c\n", i);
                    }
                }
            }
        }
    }
    else{
        printf("\nThat was not an alphabetic character....\n");
    }
    // used to test ASCII values:
    //printf("\n\n%d", original);
    // 97 - 122 lower case
    // 65 - 90 upper case
    //int test = 70;
    //printf("\n\n%c", test);
    
    return 0;
}

