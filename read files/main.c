/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on January 14, 2017, 2:33 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
int main() {

    FILE *fp;
    char data[50];
    fp = fopen("/tmp/test.txt", "r");
    
    if(fp == NULL){
        printf("File could not be opened.\n");
    } else {
        printf("File Opened.\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
        while(fgets(data, 50, fp) != NULL){
            printf("%s", data);
        }
        printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\nclosing file.\n\n");
        fclose(fp);
    }
    
    return 0;
}

