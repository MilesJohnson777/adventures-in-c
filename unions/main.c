/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on January 19, 2017, 9:59 AM
 */

#include <stdio.h>
#include <stdlib.h>


void main() {

    typedef union{
        short individual;
        float pound;
        float ounce;
    } amount;
    
    amount orangeAmt = {.ounce = 16};
    orangeAmt.individual = 4;
    
    printf("Orange juice amount: %.2f : size: %d\n\n", orangeAmt.ounce, sizeof(orangeAmt.ounce));
    
    printf("Number of oranges: %d : size: %d\n\n", orangeAmt.individual, sizeof(orangeAmt.individual));
    
    orangeAmt.pound = 1.5;
    
    printf("Weight of juice amount: %.2f : size: %d\n\n", orangeAmt.pound, sizeof(orangeAmt.pound));
    
    printf("Memory addresses for each data type:\nindividual: %d\npound: %d\nounce: %d\n",
            &orangeAmt.individual, &orangeAmt.pound, &orangeAmt.ounce);
    
    if(&orangeAmt.individual == &orangeAmt.ounce && &orangeAmt.ounce == &orangeAmt.pound){
        printf("All the memory addresses are the same, proving that unions only hold one value at a time.\n\n");
    }else{
        printf("All memory addresses are different which means unions can hold more than one value at a time.\n\n");
    }
    
    typedef enum{ INDIV, OUNCE, POUND } quantity;
    
    typedef struct{
        char *brand;
        amount theAmount;
        quantity quantityType;
    } orangeProduct;
    
    orangeProduct productOrdered = {"Chiquita", .theAmount.ounce = 16, .quantityType = POUND};
    
    printf("You bought %.2foz of %s oranges\n\n", productOrdered.theAmount.ounce, productOrdered.brand);
    
    orangeAmt.individual = 4;
    
    switch(productOrdered.quantityType){
        case INDIV:
            printf("You bought %d oranges.\n\n", orangeAmt.individual);
            break;
        case OUNCE:
            printf("You bought %.2foz of oranges.\n\n", orangeAmt.ounce);
            break;
        default:
            printf("You bought %.2flbs of oranges.\n\n", orangeAmt.pound);
            break;
    }
       
}

