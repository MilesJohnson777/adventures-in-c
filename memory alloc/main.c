/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on February 11, 2017, 2:57 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
int main() {
    
    int amtOfNum;
    
    printf("how many numbers to store: \n");
    
    scanf("%d", &amtOfNum);
    
    int * pRandomNums;
    
    pRandomNums = (int *) malloc(amtOfNum * sizeof(int));
    
    if(pRandomNums != NULL){
        int i = 0;
        
        printf("Enter a number or quit: ");
        
        while(i < amtOfNum && scanf("%d", &pRandomNums[i]) == 1){
            printf("Enter a number or quit: ");
            
            i++;
        }
        
        printf("\nYou entered the following numbers:\n");
        
        for(int j = 0; j < i; j++){
            printf("%d\n", pRandomNums[j]);
        }
    }
    free(pRandomNums); //not needed

    return 0;
}

