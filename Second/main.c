/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on October 11, 2016, 6:10 PM
 */

#include <stdio.h>

/*
 * 
 */
int main() {
    int var = 45;
    printf("var = %d\n", var);  //regular
    printf("var = %o\n", var);  //octal
    printf("var = %x\n\n", var);//hex
    
    int var1 = 056;
    printf("var1 = %d\n", var1);  //regular
    printf("var1 = %o\n", var1);  //octal
    printf("var1 = %x\n\n", var1);//hex
    
    int var2 = 0xa;
    printf("var1 = %d\n", var1);  //regular 
    printf("var1 = %o\n", var1);  //octal
    printf("var1 = %x\n", var1);  //hex
    
    return 0;
}

