/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on January 19, 2017, 3:42 PM
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct product{
    const char *name;
    _Bool plural;
    _Bool AN;
    float price;
    struct product *next;
} product;

void printLinkedList(product *pProduct){
    while(pProduct != NULL){
        if(pProduct->AN == 1 || pProduct->plural == 1){
            if(pProduct->AN == 1){
                printf("An %s costs: $%.2f.\n", pProduct->name, pProduct->price);
                pProduct = pProduct->next;
            }else{
                printf("%s costs: $%.2f.\n", pProduct->name, pProduct->price);
                pProduct = pProduct->next;
            }
        }else{
            printf("A %s costs: $%.2f.\n", pProduct->name, pProduct->price);
            pProduct = pProduct->next;
        }
    }
}

void main() {

    product tomato = {"Tomato", 0, 0, .51, NULL};
    product potato = {"Potato", 0, 0, 1.21, NULL};
    product lemon = {"Lemon", 0, 0, 1.69, NULL};
    product milk = {"Milk", 1, 0, 3.09, NULL};
    product turkey = {"Turkey", 0, 0, 1.86, NULL};
    
    tomato.next = &potato;
    potato.next = &lemon;
    lemon.next = &milk;
    milk.next = &turkey;
    
    product apple = {"Apple", 0, 1, 1.58, NULL};
    
    lemon.next = &apple;
    apple.next = &milk;
    
    printLinkedList(&tomato);
    
}

