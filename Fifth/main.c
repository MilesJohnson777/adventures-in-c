/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on October 13, 2016, 9:48 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
int main() {

    char A = 'A';
    int a = 2;
    int a2 = 22;
    int a3 = 7;
    long b = 2000000000;
    float c = 4.2500;
    double d = 30.51643;
    long double e = 1234123412341234.1234123412341234;
    
    //the result of this operation will be an integer
    printf("Character operated with an integer: %c * %d = %d, integer type vars have a higher precedence!\n\n", A, a, A*a);
    printf("I'm skipping long type variables operated with integers because there will be no significant distinction.\n\n");
    //the result of this operation will be a float
    printf("Integer operated with a floating point value: %d * %f = %f, float type vars have a higher precedence!\n\n", a, c, a*c);
    //the result of this operation will be a double
    printf("Floats operated with doubles: %f / %lf = %lf, double type vars have a higher precedence!\n\n", c, d, c/d);
    //the result of this operation will be a long double    
    printf("Doubles operated with long doubles: %f * %Lf = %Lf, long double type vars have a higher precedence!\n\n", d, e, d*e);
    float pi;
    pi = (float)a2 / a3;
    
    printf("Casting bonus with a pi approximation: %d / %d = %f", a2, a3, pi);
    return 0;
}

