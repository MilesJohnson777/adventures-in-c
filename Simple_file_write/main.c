/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on January 10, 2017, 7:16 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
int main() {

    FILE *fp;
    
    fp = fopen("tmp/test.txt", "w+");
    fprintf(fp, "here is some example text - fprintf\n");
    fputs("more example text - fputs\n", fp);
    fclose(fp);
    
    return 0;
}

