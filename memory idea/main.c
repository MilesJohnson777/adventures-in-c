/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on February 16, 2017, 11:06 AM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
void main() {

    int var1 = 45;
    int var2;
    printf("var1: %d   var1 address: %d\n", var1, &var1);
    
    var2 = var1;
    //&var2 = &var1;
    //*var2 = &var1;
    //&var2 = *var1;
    //*var2 = *var1
    
    printf("var2: %d   var2 address: %d\n", var2, &var2);
}

