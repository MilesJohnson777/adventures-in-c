/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on January 17, 2017, 11:07 AM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */
void main() {

    int varPoint = 345;
    printf("var is %d\n", varPoint);
    printf("var has hex address of: %p\n", &varPoint);
    printf("and a decimal address of: %d\n", &varPoint);
    int * pVar = &varPoint;
    printf("allocated address (pVar): %d\n", pVar);
    printf("pVar has a decimal address of: %d\n", &pVar);
    printf("and a hex address of: %p\n", &pVar);
}

