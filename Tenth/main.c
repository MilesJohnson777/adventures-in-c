/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: miles
 *
 * Created on October 20, 2016, 9:32 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 *  what is the value of End Of File??
 */
int main() {

   // printf("%d \n", EOF);
    
    double nc;
    
    for(nc =0; getchar() != EOF; ++nc)
        ;
    printf("%.0f\n", nc);
    
    
    return 0;
}

